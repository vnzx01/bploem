<div class="row">
    <div class="col s12 m12">
        <blockquote>
            Place your email used on register to send you an reset password link.
        </blockquote>
    </div>
    <div class="input-field col s12 m12 no-margin-bottom">
        <input type="email" name="reset-email" id="reset-email">
        <label for="reset-email">Email</label>
        <span class="helper-text" data-error="Please input a valid email address."></span>
    </div>
    <div class="col s12 m12">
        <p class="z-depth-1 hide" id="reset-pass-msg"></p>
    </div>
    <div class="input-field col s12 m12" id="reset-pass-loader">
        <div class="loader-container hide">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
        <button class="btn waves-effect waves-light full-width-btn" type="submit" id="reset-btn-submit">submit</button>
    </div>
    <div class="col s12 m12">
        <p>Back to <a href="#" id="back-login-btn">login</a>.</p>
    </div>
</div>
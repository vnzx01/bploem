<form autocomplete="off" role="form" id="regs-form">
    <div class="row">
        <div class="input-field col s12 m12 no-margin-bottom">
            <input type="text" class="validate txt-uppercase" name="firstname" id="regs-firstname">
            <label for="regs-firstname">First Name</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <div class="input-field col s12 m12 no-margin-bottom">
            <input type="text" class="txt-uppercase" name="middlename" id="regs-middlename">
            <label for="regs-middlename">Middle Name</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <div class="input-field col s12 m12 no-margin-bottom">
            <input type="text" class="validate txt-uppercase" name="lastname" id="regs-lastname">
            <label for="regs-lastname">Last Name</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <!--  -->
        <div class="input-field col s12 m4">
            <input type="text" class="validate" name="date-of-birth" id="regs-date-of-birth" placeholder="MM/DD/YYYY">
            <label for="regs-date-of-birth">Date of Birth</label>
            <span class="helper-text" data-error="Please input a valid date."></span>
        </div>
        <div class="input-field col s12 m5">
            <input type="email" class="validate" name="email" id="regs-email">
            <label for="regs-email">Email</label>
            <span class="helper-text" data-error="Please input a valid email address."></span>
        </div>
        <div class="input-field col s12 m3">
            <select name="gender" id="gender">
                <option value="m">Male</option>
                <option value="f">Female</option>
            </select>
            <label>Gender</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <!--  -->
        <div class="input-field col s12 m6">
            <input type="password" class="validate" name="password" id="regs-password">
            <label for="regs-password">Password</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <div class="input-field col s12 m6">
            <input type="password" class="validate" name="password_confirmation" id="regs-password-confirm">
            <label for="regs-password-confirm">Confirm Password</label>
            <span class="helper-text" data-error="This is a required field."></span>
        </div>
        <div class="col s12 m12">
            <p class="z-depth-1 form-msg hide"></p>
        </div>
        <div class="input-field col s12 m12">
            <div class="loader-container hide">
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <button class="btn waves-effect waves-light left" type="submit">Register
                <i class="material-icons right">send</i>
            </button>
        </div>
        <div class="col s12 m12">
            <p>Back to <a href="#" id="back-login-btn">login</a>.</p>
        </div>
    </div>
</form>

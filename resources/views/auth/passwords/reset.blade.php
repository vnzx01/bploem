<!DOCTYPE html>
<html lang="en">

<head>

    <title>{{ config('app.name', 'App Name') }}</title>
    <!--Import Google Icon Font-->
    <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ asset('css/app/login.css') }}">

    <meta charset="utf-8"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Parañaque City Integrated Occupational Permits System" name="description"/>
    <meta content="Lloyd M. Ababao" name="author"/>
    <meta content="Robinson Cusipag" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<style>
	.no-margin-bottom {
		margin-bottom: 0;
	}
	</style>
</head>

<body>
    <main class="no-padding">
        <div class="container">
            <div class="register-container">
                <div class="card z-depth-4">
                    <div class="card-content">
                        <div class="row no-margin-bottom" >
                            <div class="col s12 m12">
                                <span class="card-title green-text">{{ config('app.name', 'App Mo To!') }} Reset Password</span>
                                <div class="divider-bottom"></div>
                                <br>
                                <form method="POST" action="{{ route('password.request') }}">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <input type="hidden" name="email" id="email" class="validate {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ $email ?? old('email') }}" required autocomplete="email">

                                    <div class="row no-margin-bottom">
                                        <div class="input-field col s12 m12">
                                            <input type="text" name="showed-email" id="showed-email" class="validate {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ $email ?? old('email') }}" required autocomplete="email" disabled>
                                            <label for="showed-email">Email</label>
                                            @if ($errors->has('email'))
                                                <span class="helper-text" data-error="{{ $errors->first('email') }}" data-success=""></span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12 m12">
                                            @php
                                                $msg = 'This is a required field.';
                                            @endphp
                                            
                                            @if ($errors->has('password'))
                                            <?php $msg = $errors->first('password'); ?>
                                            @endif
                                            <input type="password" name="password" id="password" class="validate {{ $errors->has('password') ? 'invalid' : '' }}" required autocomplete="new-password" autofocus>
                                            <label for="password">New Password</label>
                                            <span class="helper-text" data-error="{{ $msg }}" data-success=""></span>

                                            
                                        </div>
                                        <div class="input-field col s12 m12">
                                            <input type="password" name="password_confirmation" id="password-confirm" class="validate" required autocomplete="new-password">
                                            <label for="password-confirm">Re-type New Password</label>
                                            <span class="helper-text" data-error="This is a required field."></span>
                                        </div>
                                        <div class="col s12 m12">
                                            <p class="z-depth-1 hide" id="login-msg"></p>
                                        </div>
                                        <div class="input-field col s12 m12">
                                            <div class="loader-container hide">
                                                <div class="progress">
                                                    <div class="indeterminate"></div>
                                                </div>
                                            </div>
                                            <button class="btn waves-effect waves-light left" type="submit" id="staf-login-btn">
                                                Submit <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/materialize.min.js') }}"></script>
</body>

</html>

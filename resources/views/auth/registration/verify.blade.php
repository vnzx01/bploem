<!DOCTYPE html>
<html lang="en">

<head>

    <title>{{ config('app.name', 'App Name') }}</title>
    <!--Import Google Icon Font-->
    <link href="css/icon.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ asset('css/app/login.css') }}">

    <meta charset="utf-8"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Parañaque City Integrated Occupational Permits System" name="description"/>
    <meta content="Lloyd M. Ababao" name="author"/>
    <meta content="Robinson Cusipag" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<style>
	.no-margin-bottom {
		margin-bottom: 0;
	}
	</style>
</head>

<body>
    <main class="no-padding">
        <div class="container">
            <div class="login-container">
                <div class="card z-depth-4">
                    <div class="card-content">
                        <div class="row no-margin-bottom" >
                            <div class="col s12 m12">
                                <span class="card-title {{ $color }}-text">{{ $title }}</span>
                                <p>{!! $msg !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <!--Import jQuery before materialize.js-->
</body>

</html>

<?php $is_min = (env('APP_ENV') == 'production') ? '.min' : ''; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title>{{ config('app.name', 'App Name') }}</title>
    <!--Import Google Icon Font-->
    <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app/login'.$is_min.'.css') }}">

    <meta charset="utf-8"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Parañaque City Integrated Occupational Permit System" name="description"/>
    <meta content="Lloyd M. Ababao" name="author"/>
    <meta content="Robinson Cusipag" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<style>
	.no-margin-bottom {
		margin-bottom: 0;
	}
	</style>
</head>

<body>
    <main class="no-padding">
        <div class="container">
            <div class="login-container" id="login-cntr">
                <div class="card z-depth-4">
                    <div class="card-content">
                        <div class="row no-margin-bottom" >
                            <div class="col s12 m12">
                                <div class="row">
                                    <div class="col s12 m12">
                                        <div class="login-logo-container">
                                            <div class="center">
                                                <span class="card-title green-text"><span class="teal-text text-darken-3" style="font-weight:bold; margin-bottom: 10px; display:block">Employee Portal</span> {{ config('app.name', 'App Mo To!') }}</span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <br>
                                        <form autocomplete="off" role="form" id="login-form">
                                            <div class="row no-margin-bottom">
                                                <div class="input-field col s12 m12">
                                                    <i class="material-icons prefix grey-text text-lighten-1">person</i>
                                                    <input type="text" name="username" id="username">
                                                    <label for="username">Username</label>
                                                </div>
                                                <div class="input-field col s12 m12">
                                                    <i class="material-icons prefix grey-text text-lighten-1">vpn_key</i>
                                                    <input type="password" name="password" id="password">
                                                    <label for="password">Password</label>
                                                </div>
                                                <div class="col s12 m12">
                                                    <p class="z-depth-1 hide" id="login-msg"></p>
                                                </div>
                                                <div class="input-field col s12 m12">
                                                    <div class="loader-container hide">
                                                        <div class="progress">
                                                            <div class="indeterminate"></div>
                                                        </div>
                                                    </div>
                                                    <button class="btn waves-effect waves-light left" type="submit">
                                                        Submit <i class="material-icons right">send</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.inputmask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app/employees/login'.$is_min.'.js') }}"></script>
</body>

</html>

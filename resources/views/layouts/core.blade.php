<?php
use App\Models\Pages;

$mpages =  new Pages;

// $pages = (is_super_admin()) ? $mpages->navs() : $mpages->filteredNavs();
$pages = $mpages->navs();

$active_page = $mpages->where('Name', $assoc_page_name)->first();

$plugins = isset($plugins) ? $plugins : [];
$incl = load_plugins($plugins);

$js = str_replace('.', '/', str_replace('views.', '', $views));

$is_min = (env('APP_ENV') == 'production') ? '.min' : '';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ config('app.name', 'App Name') }}
        @if(!empty($active_page->Name))
        {{ '| '.$active_page->Label }}
        @endif
    </title>
    <!--Import Google Icon Font-->
    <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection" />
    <!-- <link type="text/css" rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" /> -->
    <!-- plugins -->
    {!! !empty($incl['css']) ? implode('', $incl['css']) : false !!}
    <!-- end -->
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/layout'.$is_min.'.css') }}">
    @if(!empty($css))
        @foreach($css as $c)
        <link rel="stylesheet" href="{{ asset('css/app/'.$c.$is_min.'.css') }}">
        @endforeach
    @endif
    <meta charset="utf-8"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Parañaque City Integrated Occupational Permit System" name="description"/>
    <meta content="Lloyd M. Ababao" name="author"/>
    <meta content="Robinson Cusipag" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <header>
        @include('layouts.side-nav', ['pages' => $pages])
    </header>
    <main class="">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    @include($views.'index')
                </div>
            </div>
        </div>
        <div id="modal-main" class="modal core-modal">
            <div class="modal-content"></div>
            <div class="divider"></div>
            <div class="modal-footer">
                <a class="waves-effect modal-ok btn-flat green-text"><b><i class="material-icons left">done</i>Ok</b></a>
                <a class="waves-effect modal-close btn-flat"><b><i class="material-icons left">replay</i>Cancel</b></a>
            </div>
        </div>
        <div id="dialog-box-modal" class="modal modal-dialog-box">
            <div class="modal-content">
                <h6 class="no-margin-top"><span></span></h6>
                <p class="no-margin-bottom"></p>
            </div>
            <div class="modal-footer">
                <div class="right">
                    <a href="#!" class="waves-effect waves-default btn-flat dialog-confirm">Yes</a>
                    <a href="#!" class="waves-effect waves-default btn-flat modal-close dialog-close">Cancel</a>
                </div>
            </div>
        </div>
    </main>
    <footer class="page-footer">
      <div class="footer-copyright">
        <div class="container-footer center">© {{ date('Y')}} {{ config('app.name', 'App Name') }}</div>
      </div>
    </footer>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
    <!-- App Plugins -->
    {!! !empty($incl['js']) ? implode('', $incl['js']) : false !!}
    <!-- End -->
    <script type="text/javascript" src="{{ asset('js/plugins/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/require.js') }}"></script>
    <script type="text/javascript">
        const full_url = '{{ url()->current() }}';
        const base_url = '{{ url('/') }}';
        const app_js = '{{ url("js/app") }}/';
        const app_plugins = '{{ url("js/plugins") }}/';
        const is_min = '{{ $is_min }}';
    </script>
    <script type="text/javascript" src="{{ asset('js/app/main'.$is_min.'.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app/'.$js.'index'.$is_min.'.js') }}"></script>
</body>

</html>

<?php

if (auth('staff')->check()) {
    $hurl =  '/employees';
    $lurl = 'emp.logout';
    
} else {
    $hurl =  '/';  
    $lurl = 'logout';

}

?>
<div class="navbar-fixed show-on-med-and-down hide-on-large-only">
    <nav>
        <div class="nav-wrapper">
            <a href="{{ url($hurl) }}" class="brand-logo">
                {{ config('app.name', 'App Name') }}
                <!-- <img class="responsive-img" src="img/pt-hz.jpg"> -->
            </a>
            <a href="#" data-target="mobile-nav" class="sidenav-trigger right"><i class="material-icons">reorder</i></a>
        </div>
    </nav>
</div>  
<ul id="mobile-nav" class="sidenav sidenav-fixed">
    <li class="logo exclude-hover">
        <a href="{{ url($hurl) }}" class="brand-logo center">   
            @if (auth('staff')->check())
            <span class="teal-text text-darken-3" style="font-weight:bold; margin-bottom: 10px; display:block">Employee Portal</span>
            @endif
            {{ config('app.name', 'App Name') }}
            <!-- <img class="responsive-img" src="img/pt-hz.jpg"> -->
        </a>
    </li>
    <div class="divider"></div>
    <li class="info exclude-hover">
        <div class="log-user-container">
            <!-- <a href="#" class="dropdown-trigger" data-target="log-setting2"> -->
            <?php $path = (auth('staff')->check()) ? 'employees/profile/photo/' : 'profile/photo/'; ?>
            <img src="{{ url($path.md5(logged_id())) }}" class="circle responsive-img sidenav-profile-photo" style="width: 40px;" />
            <span class="logged-name">{{ ucwords(logged_name()) }}</span>
        </div>
    </li>
    <div class="divider"></div>
    @if(!empty($pages))
    <ul class="collapsible">
    @foreach($pages as $mkey => $_this)
        <?php
        $subs = !empty($_this['sub']) ? 'sidenav-sub' : '';
        $par_pg_id = ($active_page->IsParent) ? $active_page->PageID : $active_page->ParentPageID;
        ?>
        <li class="{{ link_active($par_pg_id, $mkey) }}">
            <a href="{{ url($_this['url']) }}" class="collapsible-header waves-effect waves-light {{ $subs }}" >
                <i class="material-icons left">{{ $_this['icon'] }}</i>{{ $_this['label'] }}
            </a>
            @if(!empty($_this['sub']))
            <div class="collapsible-body">
                <ul>
                    @foreach($_this['sub'] as $sub)
                    <li class="{{ link_active($active_page->PageID, $sub['sub_id']) }}">
                        <a href="{{ url($_this['url'].$sub['url']) }}" class="waves-effect waves-light">
                            <i class="material-icons left">{{ $sub['icon'] }}</i>{{ $sub['label'] }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </li>
    @endforeach
        <li>
            <a class="collapsible-header waves-effect waves-light" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="material-icons left">power_settings_new</i>Log Out
            </a>
            <form id="logout-form" action="{{ route($lurl) }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
    @endif
</ul>

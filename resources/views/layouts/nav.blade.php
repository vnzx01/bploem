<?php
use App\Models\Pages;

$pages =  new Pages;

$xpl = explode('.', $module_name);
// print_result($pages->navs());
// die();
?>
<nav class="show-on-med-and-down hide-on-large-only">
    <div class="nav-wrapper">
        <a href="{{ url('/') }}" class="brand-logo">
            {{ config('app.name', 'App Name') }}
            <!-- <img class="responsive-img" src="img/pt-hz.jpg"> -->
        </a>
        <a href="#" class="right waves-effect nav-scroll-btn hide-on-med-and-down" data-pos="right"><i class="material-icons">chevron_right</i></a>
        <div class="log-user-container hide-on-med-and-down right hide">
            <a href="#" class="dropdown-trigger" data-target="log-setting">
                <img src="{{ asset('img/profile.png') }}" class="circle responsive-img" />
            </a>
            <ul id="log-setting" class="dropdown-content">
                <li class="{{ link_active($module_name, 'profile') }}">
                    <a href="{{ url('/profile') }}" class="center-align"><i class="material-icons center">perm_identity</i>Profile</a>
                </li>
                <li class="{{ link_active($module_name, 'settings') }}">
                    <a href="{{ url('/settings') }}" class="center-align"><i class="material-icons center">settings_applications</i>Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <!-- <a href="{{ url('/logout') }}" class="center-align"><i class="material-icons center">power_settings_new</i>Log Out</a> -->
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="center-align"><i class="material-icons center">power_settings_new</i>Log Out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        <ul id="nav-mobile" class="hide-on-med-and-down main-nav">
        @if(!empty($pages->navs()))
        @foreach($pages->navs() as $_this)
        <?php $subs = !empty($_this['sub']) ? 'dropdown-trigger' : '';  ?>
        <li class="{{ link_active($xpl[0], $_this['module']) }} ">
          <a href="{{ url($_this['url']) }}" class="{{ $subs }}" data-target="{{ $_this['module'] }}">
              @if(str_is('fa-*', $_this['icon']))
              <i class="fa {{ $_this['icon'] }} left "></i>{{ $_this['label'] }}
              @else
              <i class="material-icons left">{{ $_this['icon'] }}</i>{{ $_this['label'] }}
              @endif
          </a>
          @if(!empty($_this['sub']))
          <ul id="{{ $_this['module'] }}" class="dropdown-content main-menus">
              @foreach($_this['sub'] as $sub)
              <?php $sub_module_name = (!empty($xpl[1]) ? $_this['module'].'_'.$xpl[1] : $_this['module']); ?>
              <li class="{{ link_active($sub_module_name, $sub['module']) }}">
                <a href="{{ url($_this['url'].$sub['url']) }}">
                    @if(str_is('fa-*', $sub['icon']))
                    <i class="fa {{ $sub['icon'] }}"></i>{{ $sub['label'] }}
                    @else
                    <i class="material-icons left">{{ $sub['icon'] }}</i>{{ $sub['label'] }}
                    @endif
                </a>
              </li>
              @endforeach
          </ul>
          @endif
        </li>
        @endforeach
        @endif
        <!-- <li class="divider-left"> -->
        <!-- </li> -->
      </ul>
        <a href="#" class="right waves-effect nav-scroll-btn hide-on-med-and-down hide" data-pos="left"><i class="material-icons">chevron_left</i></a>
        <a href="#" data-target="mobile-nav" class="sidenav-trigger right"><i class="material-icons">reorder</i></a>
    </div>
</nav>
@include('layouts.side-nav', ['pages' => $pages->navs()])

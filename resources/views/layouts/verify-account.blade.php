<!DOCTYPE html>
<html lang="en">

<head>

    <title>{{ config('app.name', 'App Name') }} Account Information</title>
    <!--Import Google Icon Font-->
    <!-- <link href="css/icon.css" rel="stylesheet"> -->
    <!--Import materialize.css-->
	<link href="{{ asset('css/icon.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ asset('css/app/login.css') }}">

    <meta charset="utf-8"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Parañaque City Integrated Occupational Permits System" name="description"/>
    <meta content="Lloyd M. Ababao" name="author"/>
    <meta content="Robinson Cusipag" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<style>
	.no-margin-bottom {
		margin-bottom: 0;
	}
	
	.left-txt{
		 text-align: left;
		 margin-bottom: 30px;
	
	}
	</style>
</head>

<body>

    <main class="no-padding">
	

        <div class="container center">
		<div class=" row "> 
			<h3>
				City of Parañaque Integrated Occupational Permit 
			</h3>
			
		</div>
            <div class="row center" >
                <div class="col s12 m10 l4" width="10px">
                    <div class="card-panel verify-bx">
						<div class="row left-txt"> Key-in your Occupational ID Number to check the status.</div>
                        <div class="row  input-field row">
						
                            <input placeholder="Place your ID number here" id="occu_id" type="text" class="validate" >
                            <label for="occu_id">Occupational ID</label>
                        </div>
                         <button class="btn waves-effect waves-light "> <i class="material-icons">verified_user</i>  verify  </button>
                    </div>
                </div>
                
				<!--
				<div class="col s12 m8 l8">
                    <div class="card-panel verify-bx">
                        <span class="card-title"></span>
                        <!-- dito ka mag lagay ng content!!! 
                    </div>  -->
                </div> 
			</div>
        </div>
    </main>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/materialize.min.js') }}"></script>
</body>

</html>

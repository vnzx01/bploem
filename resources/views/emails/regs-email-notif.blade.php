@component('mail::message')
<h1>Greetings {{ $fullname }}!</h1>
We just need to verify that <span style="font-weight: bold">{{ $email }}</span> is your email address,  this will be an important part of the steps that you need to do in order for you to complete the process in securing your Occupational Work Pemit.

@component('mail::button', ['url' => url('verify/code='.$verify_token)])
Verify Email
@endcomponent

Didn't request this email verification? Just ignore or delete this email, your email might've been entered by mistake.

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
<center style="font-size: 12px">City Government of Parañaque, San Antonio Valley I, San Antonio, Parañaque City <br>Powered by SynergyLabsPH</center>
@endcomponent

@endcomponent

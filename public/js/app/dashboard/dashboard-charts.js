define(function(e){
    var charts = {

        run: function(){

            var randomScalingFactor = function() {
                return Math.round(Math.random() * 29);
                //return 0;
            };
            var randomColorFactor = function() {
                return Math.round(Math.random() * 255);
            };
            var randomColor = function(opacity) {
                return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
            };

            var config = {
                type: 'line',
                data: {
                    labels: ["1", "2", "3", "4", "5", "6", "7", '8', '9', '10'],
                    datasets: [{
                        label: "Male",
                        data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
                        // fill: false,
                        // borderDash: [5, 5],
                        borderColor: 'rgba(50,136,189,0.4)',
                        backgroundColor: 'rgba(113,172,188,0.5)',
                        pointBorderColor: 'rgba(55,182,155,0.7)',
                        pointBackgroundColor: 'rgba(113,172,188,0.5)',
                        pointBorderWidth: 1,
                    }, {
                        label: "Female",
                        data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
                        // fill: false,
                        borderColor: 'rgba(244,109,67,0.4)',
                        backgroundColor: 'rgba(213,62,79,0.5)',
                        pointBorderColor: 'rgba(253,174,97,0.7)',
                        pointBackgroundColor: 'rgba(213,62,79,0.5)',
                        pointBorderWidth: 1,
                    }]
                },
                options: {
                    responsive: true,
                    scaleOverride:true,
                    // title:{
                    //     display:true,
                    //     text:'Chart.js Line Chart'
                    // },
                    tooltips: {
                        mode: 'label',
                        callbacks: {
                            // beforeTitle: function() {
                            //     return '...beforeTitle';
                            // },
                            // afterTitle: function() {
                            //     return '...afterTitle';
                            // },
                            // beforeBody: function() {
                            //     return '...beforeBody';
                            // },
                            // afterBody: function() {
                            //     return '...afterBody';
                            // },
                            // beforeFooter: function() {
                            //     return '...beforeFooter';
                            // },
                            // footer: function() {
                            //     return 'Footer';
                            // },
                            // afterFooter: function() {
                            //     return '...afterFooter';
                            // },
                        }
                    },
                    hover: {
                        mode: 'dataset'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Days in January'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Total'
                            },
                            ticks: {

                                beginAtZero: true,
                                max: 30,
                            }
                        }]
                    }
                }
            };

            var ctx = document.getElementById("canvas").getContext("2d");
            var _chart = new Chart(ctx, config);
        }
    }

    return charts;
});

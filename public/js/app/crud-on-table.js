"use strict";

define(function(e) {

    var CrudOnTable = {
        def_tblID: 'rec-table',
        def_formID: 'crud-tbl-form',

        save: function(form_id, id, custom_data) {
            var _this = this,
                form_id = (form_id) ? form_id : this.def_formID,
                id = (id) ? id : 0,
                action = (id == 0 ) ? 'add' : 'edit',
                d = (custom_data) ? custom_data : $('#' +form_id).serialize();

            reset_form_errors();

            do_ajax({
                    'url': full_url + '/event',
                    data: 'action=' + action + '&' + d + '&id=' + id,
                    method: 'POST'
                }, '', '',
                function(data) {
                    do_loader('hide');

                    if (data.error == false) {

                        notif_alert('success', data.message);

                        if (action == 'add') {
                            reset_form_fields();
                            var dt = $('#' + _this.def_tblID).DataTable();

                            dt.row.add($(data.row)).draw(false);

                        } else if (action == 'edit') {
                            var tr = $('tr[data-id='+id+']');

                            tr.find('.td-editable').each(function(i,v) {
                                var vl = $(this).find('input').val();

                                $(this).find('span:not(.helper-text)').text(vl);
                            });

                            tr.find('.tde-btn-edit-cancel').click();
                        }
                    }
                }, function(err) {
                    if(err.responseJSON) {
                        if(action == 'add') {
                            parse_error(err);

                        } else {
                            var err_data = err.responseJSON.data;
                            for (var _key in err_data) {
                                $('tr[data-id='+id+'] input[name=' + _key+']').addClass('invalid invalid-with-val').next().attr('data-error', err_data[_key]);
                            }
                        }
                    } else {
                        notif_alert('error', 'Oops! ' + err.statusText);
                    }
                });
        },

        remove: function(id) {
            var id = (id) ? id : 0,
                _this = this;

            do_ajax({
                    'url': full_url + '/event',
                    data: {'action': 'delete', 'id': id},
                    method: 'POST'
                }, '', '',
                function(data) {
                    do_loader('hide');

                    if (data.error == false) {
                        var tr = 'tr[data-id='+ id +']';

                        notif_alert('success', data.message);

                        animate_elem(tr, '', function(){
                            $('#' + _this.def_tblID).DataTable().row($(tr)).remove().draw();
                            confirm_box_close();
                        });

                    } else {
                        notif_alert('error', data.message);
                    }
                });

        },

        run: function() {

            custom_datatable(CrudOnTable.def_tblID);

            // actions
            $('body').on('click', '#add-new-btn', function(e) {
                e.preventDefault();

                if (form_validate(CrudOnTable.def_formID)) {
                    CrudOnTable.save();
                }
            });

            $('body').on('click', '.tde-btn-edit-save', function(e){
                var id = $(this).attr('data-id'),
                    tr = $(this).closest('tr'),
                    d = [];

                tr.find('.td-editable').each(function(i, v) {
                    var dt = $(this).find('input');
                    d.push(dt.attr('name') + '=' +dt.val());
                });

                CrudOnTable.save('', id, d.join('&'));
            });

            $('body').on('click', '.tde-btn-delete', function(e) {
                var id = $(this).attr('data-id');

                confirm_box('remove', '', '', function(){
                    CrudOnTable.remove(id);
                });
            });
            // End

            $('body').on('click', '#cancel-btn', function(e){
                reset_form_fields();
            });

            $('body').on('click', '.tde-btn-edit', function(e) {
                var tds = $(this).closest('tr').find('.td-editable');

                tds.each(function() {
                    $(this).find('span:not(.helper-text)').addClass('hide').next().removeClass('hide');
                });

                $(this).parent().addClass('hide').next().removeClass('hide');
            });

            $('body').on('click', '.tde-btn-edit-cancel', function(e) {
                var tds = $(this).closest('tr').find('.td-editable');

                tds.each(function() {
                    $(this).find('input[type=text]').addClass('hide').prev().removeClass('hide');
                });

                $(this).parent().addClass('hide').prev().removeClass('hide');
            });

        }
    };

    return CrudOnTable;
});

"user strict";

function trans_div(action, div_ids, cb) {

    var _ids, trans;

    switch (action) {
        case 'show':
            trans = 'fadeInRight';
            break;
        case 'hide':
            trans = 'fadeOutRight';
            break;
    }

    if (div_ids instanceof Array) {
        for (var a = 0; a < div_ids.length; a++) {
            div_ids[a] = '#' + div_ids[a];
        }
        _ids = div_ids.join();
    } else {
        _ids = '#' + div_ids;
    }

    $(_ids).addClass(trans).delay(150).queue(function() {
        switch (action) {
            case 'show':
                $(this).removeClass('hide');
                break;
            case 'hide':
                $(this).removeClass(trans);
                $(this).addClass('hide');
                break;
        }

        (cb !== undefined) ? cb(): false;
        $(this).dequeue();
    });

}

function parse_error(err){
    if(err !== undefined || err instanceof Object){
        var err_json = err.responseJSON, is_error = err_json.error, err_data = err_json.data;
        if(is_error){
            for (var _key in err_data) {
                var _id = $('#' + _key);

                if (_id.is('select')) {
                    var par = _id.closest('.input-field'),
                        el = par.find('.helper-text').attr('data-error', err_data[_key]);
                        
                    par.find('.select-dropdown').addClass('invalid invalid-with-val').parent().find('.helper-text').remove().end().append(el[0].outerHTML);

                } else {
                    _id.addClass('invalid invalid-with-val').parent().find('.helper-text').attr('data-error', err_data[_key]);

                }
            }
        }
    }
}

var Login = function(username, password) {
    var loader_div = $('#login-cntr .loader-container'),
        loging_msg_elem = $('#login-msg');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        url: window.location.href,
        type: 'POST',
        data: 'email=' + username + '&password=' + password,
        beforeSend: function(bs) {
            loader_div.removeClass('hide');
            loging_msg_elem.addClass('hide');
        },
        success: function(data) {
            var m = loging_msg_elem.removeClass('form-error form-success hide');

            loader_div.addClass('hide');
            
            m.addClass('form-success').text(data.msg);
            window.location.href = data.url;

        },
        error: function(err) {
            loader_div.addClass('hide');
            var err_msg;

            switch (err['status']) {
                case 422:
                    let err_data = $.parseJSON(err.responseText);
                    // err_msg = err_data.message;
                    if (err_data.errors.email) {
                        err_msg = err_data.errors.email[0];
                        
                    } else {
                        err_msg = err_data.errors.password[0];
                        
                    }
                    break;
                case 404:
                    err_msg = err.statusText;
                    break;
                case 419:
                    err_msg = 'Unable to perform action. Please try to reload the page.';
                    break;
                case 500:
                    parse_error(err);
                    err_msg = "Oops! There's something wrong.";
                    break;
            }

            loging_msg_elem.addClass('form-error').removeClass('hide').text(err_msg);
        }
    });
}


$(document).ready(function(e) {
    // $('select').formSelect();

    // $('#regs-date-of-birth').inputmask("99/99/9999",{"inputFormat": "mm/dd/yyyy", 'placeholder': 'MM/DD/YYYY' });

    // $('#regs-date-of-birth').on('focus', function(e){
    //     $(this).datepicker('open');
    // }).datepicker();

    $('#login-form').on('submit', function(e) {
        e.preventDefault();
        var u = $('#username'),
            p = $('#password'),
            u_val = u.val(),
            p_val = p.val();

        if (u_val == '') {
            u.addClass('invalid');

        } else if (p_val == '') {
            p.addClass('invalid');

        } else if (u_val != '' && p_val != '') {
            Login(u_val, p_val);
        }
    });

    $('body').on('focus', '#username, #password', function() {
        $(this).removeClass('invalid');
    });

});

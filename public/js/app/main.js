"use strict";

// requirejs configs
require.config({
    // change urlArgs to "prod=v2" for prod.
    urlArgs: "dev=" + (new Date()).getTime()
});
// end

// Customized Datatable
function custom_datatable(_id, configs) {
    var elem = (_id == '' || _id == undefined || _id == 0) ? '.datatable' : '#' + _id;
    var def_cgfs = {
        "aaSorting" : [],
        "language": {
            "paginate": {
                "previous": "<i class='material-icons'>chevron_left</i>",
                "next": "<i class='material-icons'>chevron_right</i>",
            },
            "lengthMenu": "Showed entries _MENU_"
        },
    };

    if(Boolean(configs)){
        for(var obj_key in configs){
            def_cgfs[obj_key] = configs[obj_key];
        };
    }

    var dt = $(elem).DataTable(def_cgfs);

    $('table' + elem).wrap('<div class="table-scroll table-bordered"></div>');
    $('.dataTables_length select').formSelect();
    // if($(elem).hasClass('table_scroll')){
    //     $('.dataTables_length').after(dt);
    // }
    return dt;
}
// End

// Modals
function do_modal(id, main_content, trigger_open, footer_content, is_fixed_footer) {
    var _modal = $('.modal.core-modal'),
        def_id = 'modal-main';

    if (id !== undefined || id !== '') {
        def_id = id;
        _modal.attr('id', id);
    } else {
        _modal.attr('id', def_id);
    }

    if (is_fixed_footer == true) {
        _modal.addClass('modal-fixed-footer');
    } else {
        _modal.removeClass('modal-fixed-footer');
    }

    _modal.find('.modal-content').html(main_content);

    if(Boolean(footer_content)){
        _modal.find('.modal-footer').html(footer_content);
    }

    if (trigger_open == true) {
        $('#' + def_id).modal();
        $('#' + def_id).modal('open');
    }
}
// End

// Ajax Setup/Request configs
function do_loader(loader, action){
    if(Boolean(loader)){
        switch (action) {
            case 'hide':
                $(loader).addClass('hide');
                break;
            default:
                $(loader).removeClass('hide');
                break;
        }
    } else {
        switch (action) {
            case 'hide':
                preloader('hide');
                break;
            default:
                preloader('show');
                break;
        }
    }
}

function do_ajax(configs, loader, bsend_cb, succes_cb, err_cb) {
    var new_cgfs = {};
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    for(var obj_key in configs){
        new_cgfs[obj_key] = configs[obj_key];
    };

    if(Boolean(bsend_cb)){
        new_cgfs['beforeSend'] = function(){ do_loader(loader); bsend_cb() };
    } else {
        new_cgfs['beforeSend'] = function(){ do_loader(loader); };
    }

    if(Boolean(succes_cb)){
        new_cgfs['success'] = function(data){ do_loader(loader, 'hide'); succes_cb(data) };
    } else {
        new_cgfs['success'] = function(data){ do_loader(loader, 'hide'); return data; };
    }

    if(Boolean(err_cb)){
        new_cgfs['error'] = function(err) { do_loader(loader, 'hide'); err_cb(err) };
    } else {
        new_cgfs['error'] = function(err){
            do_loader(loader, 'hide');
            // console.log(err); return true;
            if(err.responseJSON !== undefined){
                parse_error(err);
                notif_alert('error', 'Oops! ' + err.statusText);

            } else {
                var err_text = '';
                if(err.responseText !== undefined || err.responseText != ''){
                    if($.parseHTML(err.responseText)[5] !== undefined){
                        err_text = $($.parseHTML(err.responseText)[5]).find('h2 span.exception_title abbr').text();
                    } else {
                        err_text = err.responseText;
                    }
                } else {
                    err_text = err.statusText;
                }

                notif_alert('error', 'Oops! ' + err_text);
            }
        };
    }

    $.ajax(new_cgfs);
}
// End

// Div transitions
function trans_div(action, div_ids, cb) {

    var _ids, trans;

    switch (action) {
        case 'show':
            trans = 'fadeInRight';
            break;
        case 'hide':
            trans = 'fadeOutRight';
            break;
    }

    if (div_ids instanceof Array) {
        for (var a = 0; a < div_ids.length; a++) {
            div_ids[a] = '#' + div_ids[a];
        }
        _ids = div_ids.join();
    } else {
        _ids = '#' + div_ids;
    }

    $(_ids).addClass(trans).delay(150).queue(function() {
        switch (action) {
            case 'show':
                $(this).removeClass('hide');
                break;
            case 'hide':
                $(this).removeClass(trans);
                $(this).addClass('hide');
                break;
        }

        (cb !== undefined) ? cb(): false;
        $(this).dequeue();
    });

}
// End

// Alerts & Messages
function notif_alert(type, msg, duration, callback) {
    var _msg, style,
        dur = (duration != undefined) ? duration : 4000,
        cb = (callback != undefined) ? callback : '';

    switch (type) {
        case 'error':
            _msg = (msg != undefined) ? msg : "Unable to proccess request.",
                style = 'form-error';
            break;
        case 'success':
            _msg = (msg != undefined) ? msg : "Request success.",
                style = 'form-success';
            break;
        default:
            _msg = (msg != undefined) ? msg : "Done with request.",
                style = 'form-primary';
            break;
    }
    M.toast({'html': _msg, 'displayLength': dur, 'classes': style, 'completeCallback': cb});
}

var notif_msg = {
    show: function(type, msg_holder_id, msg, cb){
        var _msg, style;
        switch (type) {
            case 'error':
                _msg = (msg != undefined) ? msg : "Unable to proccess request.",
                    style = 'form-error';
                break;
            case 'success':
                _msg = (msg != undefined) ? msg : "Request success.",
                    style = 'form-success';
                break;
            default:
                _msg = (msg != undefined) ? msg : "Done with request.",
                    style = 'form-primary';
                break;
        }

        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $('#' + msg_holder_id).removeClass('hide').addClass('animated fadeIn ' + style).on(animationEnd, function(){
            $(this).removeClass('animated fadeIn hide');
        }).find('p').text(msg);
    },

    hide: function(msg_holder_id){
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $('#' + msg_holder_id).addClass('animated fadeOut').on(animationEnd, function(e){
            $(this).addClass('hide');
            $(this).removeClass('animated fadeOut');
        });
    }
}

function confirm_box(setup, title, msg, cb) {
    var _msg, style,
        diag_modal = $('#dialog-box-modal'),
        // dur = (duration != undefined) ? duration : 4000,
        // cb = (callback != undefined) ? callback : '',
        d = '';

    var types = {
        'remove': {
            'title': (title) ? title : 'Warning!',
            'title_class': 'red-text',
            'msg': (msg) ? msg : 'Remove this data?',
        }
    }

    d = (typeof setup !== 'object') ? types[setup] : setup;

    diag_modal.find('h6 span').text(d.title).removeClass().addClass(d.title_class).end().find('p').text(d.msg);

    diag_modal.modal({
        onOpenStart: function(e) {
            $(e).find('.dialog-confirm').off('click').bind('click', function(ev){
                (cb) ? cb() : console.log('click!');
            });
        }
    });
    diag_modal.modal('open');
}

function confirm_box_close(cb)
{
    if(cb) {
        cb();
    };
    $('#dialog-box-modal').modal('close');
}

var preloader = function(action, text){
    text = (Boolean(text)) ? text : 'Please wait.';

    switch (action) {
        case 'show':
            var l = '<div class="loader-container right '+ action +'"><div class="preloader-wrapper small active"><div class="spinner-layer spinner-white-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><p>'+ text +'</p>';
            M.toast({'html': l, 'displayLength': 60000});
            break;

        case 'hide':
            M.Toast.dismissAll();
            break;
    }

};
// End

// Add table row
var table_row = {
    add: function(table_row) {
        var _clone;

        _clone = table_row.clone().find('div.waves-ripple').remove().end().find('input[type="text"]').val('').end();
        table_row.closest('table').append(_clone);
    },

    remove: function(table_current_row) {
        var _parent = table_current_row.parent();

        if(_parent.children().length == 1 || _parent.find('input:not(.hide)').length == 1){
             notif_alert(null, 'Unable to remove this row.');
        } else {
            table_current_row.remove();
        }
    }
}
// End

// Div animations
function animate_elem(elem, transition, cb){
    transition = (Boolean(transition)) ? transition : 'fadeOutRight';

    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $(elem).addClass('animated ' + transition).on(animationEnd, cb);
}
// End

// FAB Change btns
function fab_change(show_class, hide_class) {
    $('.' + hide_class).addClass('hide');
    $('.' + show_class).removeClass('hide');
}
// End

// check form
function form_validate(form_id){
    var required_elem = $('#' + form_id).find('*[required]'), ret = true;
    required_elem.each(function(e){
        var _this = $(this), _val = _this.val();

        if(Boolean(_val) !== true){
            ret = false;
            _this.addClass('invalid');

        } else if(Boolean(_val) === true && _this.is('input[type="email"]')){
            var e_test = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
            if(e_test.test(_val) == false){
                ret = false
                _this.addClass('invalid invalid-with-val');
            }
        }
    });
    return ret;
}

// parse error
function parse_error(err){
    if(err !== undefined || err instanceof Object){
        var err_json = err.responseJSON, is_error = err_json.error, err_data = err_json.data;
        if(is_error){
            for (var _key in err_data) {
                var _id = $('#' + _key);

                if (_id.is('select')) {
                    var par = _id.closest('.input-field'),
                        el = par.find('.helper-text').attr('data-error', err_data[_key]);
                        
                    par.find('.select-dropdown').addClass('invalid invalid-with-val').parent().find('.helper-text').remove().end().append(el[0].outerHTML);

                } else {
                    _id.addClass('invalid invalid-with-val').parent().find('.helper-text').attr('data-error', err_data[_key]);

                }
            }
        }
    }
}
// End

// Resetting form fields
function reset_form_fields() {
    $('form').removeAttr('data-id');
    $('.reset-val').val('').text('').removeClass('invalid valid invalid-with-val');
    $('.reset-currency').val('0.00').text('0.00');
    $('.reset-select option:first').attr('selected', true);
    // $('.reset-check').attr('checked', false);
    M.updateTextFields();
}

function reset_form_errors() {
    $('.input-field input, .td-editable input').removeClass('invalid valid invalid-with-val');
}
// End

$(document).ready(function(e) {

    // Navs
    $('.sidenav').sidenav({ edge: 'left'});

    $('.collapsible').collapsible();

    $('body').on('click', '.sidenav-sub', function(e){
        e.preventDefault();
    });
    // $('body').on('click', '.nav-scroll-btn', function(e) {
    //     e.preventDefault();
    //     var pos = $(this).attr('data-pos'),
    //         lef = $('.nav-scroll-btn[data-pos="left"]'),
    //         mn = $('.main-nav'),
    //         curpos = mn.scrollLeft(),
    //         scroll_speed = 200,
    //         tp = (pos == 'left') ? curpos - scroll_speed : scroll_speed + curpos;
    //
    //     mn.animate({'scrollLeft': tp}, {complete: function(){ (tp <= 0) ? lef.addClass('hide') : lef.removeClass('hide')  }});
    // });
    // End Navs

    $('.dropdown-trigger').dropdown({
        coverTrigger: false,
        hover: true,
        constrainWidth: false,
    }).on('click', function(e) {
        e.preventDefault();
    });

    $('.dropdown-trigger-click').dropdown({
        coverTrigger: false,
        hover: false,
        constrainWidth: false,
    }).on('click', function(e) {
        e.preventDefault();
    });

    $('.tooltipped').tooltip({ delay: 10 });

    $('body').on('focus', 'input[required], textarea[required]', function(e){
        $(this).removeClass('invalid invalid-with-val');
    });

    // Keypress Filters
    $("body").on('keypress', '.num-only', function(e) {
        var pattern = /^[0-9-()-/\- ]*$/;
        var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        // console.log(pattern.test($(this).val()));

        if (pattern.test(key) === true) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });
    // End


    // Datepicker configs
    $('.datepicker').on('focus', function() {
        $(this).pickadate('open');
    });

    $('.datepicker').datepicker({
        selectMonths: true,
        selectYears: true,
        format: 'mmmm dd, yyyy',
        container: 'body',
        onClose: function() {
            $(document.activeElement).blur();
        }
    });
    // End

    // Checkboxes
    $('body').on('click', '.check-parent', function(e){
        if($(this).is(':checked')){
            $('.check-child').prop('checked', true);

        } else {
            $('.check-child').prop('checked', false);
        }
    });

    $('body').on('click', '.check-child', function(e){
        if($('.check-child:checked').length === $('.check-child').length) {
            $('.check-parent').prop('checked', true);
        } else {
            $('.check-parent').prop('checked', false);
        }
    });
    // end

    // collection
    // $('body').on('click', '.collection li.collection-item', function (e) {
    //     e.preventDefault();
    //     $(this).parent().find('.collection-item.active').removeClass('active');
    //     $(this).addClass('active');
    // });
    // End

});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CustomModel;

class Position extends Model
{
    use CustomModel;

    protected $table = 'position';
    protected $primaryKey = 'id';

    protected $fillable = ['dept_id', 'position_code', 'position_name', 'position_desc', 'created_by', 'updated_by'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Traits\CustomModel;

class Reports extends Model
{
    // use CustomModel;

    protected $table = 'reports';
    protected $primaryKey = 'id';

    protected $fillable = ['rpt_name', 'rpt_group', 'rpt_path'];

}

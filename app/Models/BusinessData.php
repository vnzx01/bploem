<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Traits\CustomModel;

class BusinessData extends Model
{
    // use CustomModel;

    protected $table = 'businessdata';
    protected $primaryKey = 'EntryID';

    protected $fillable = ['BIN', 'BusinessName', 'BusinessAddress', 'EmployeeCount', 'BusinessBrgy'];

}

<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employees extends Authenticatable
{
    use Notifiable;

    protected $table = 'users_emp';

    protected $primaryKey = 'id';

    protected $guard = 'staff';

    protected $fillable = ['emp_user', 'emp_lname', 'emp_fname', 'emp_mname', 'emp_post', 'emp_dept'];

    protected $hidden = ['emp_pass', 'remember_token'];


}

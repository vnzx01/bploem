<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchedEvents extends Model
{
    protected $table = 'occu_sched_events';
    protected $primaryKey = 'id';

    protected $fillable = ['EventTypeID', 'Caption', 'DateFrom', 'DateTo', 'Days'];

}

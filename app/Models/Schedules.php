<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Schedules extends Model
{
    protected $table = 'occu_sched';
    protected $primaryKey = 'trans_id';

    protected $fillable = ['taxpayer_id', 'ref_no', 'dt_sched', 'proccess_date', 'trans_type_id', 'app_type_id'];

    public $timestamps = false;

    private $max_slot = 200;

    public $days_interval = 1;

    public function getSchedules($month, $year)
    {
        $rd = [];
        $date_skip_count = 0;
        $today = date('Y-m-d');
        $total_days = cal_days_in_month(0, $month, $year);

        $day_count = 0;

        $sched = $this->whereMonth('dt_sched', '=', $month)->whereYear('dt_sched', '=', $year)->get()->groupBy('dt_sched')->toArray();
        
        $reserve = $this->whereMonth('dt_sched', '=', $month)->whereYear('dt_sched', '=', $year)->where('taxpayer_id', logged_id())->get()->keyBy('dt_sched')->toArray();
        $reserve_dt = array_column($reserve, 'dt_sched');

        $events = $this->getMonthEvents($month, $year);

        for ($a = 1; $a <= $total_days; ++$a) {
            $date = $year.'-'.$month.'-'.sprintf('%02d', $a);
            $day = date('D', strtotime($date));

            if (!empty($events[$date])) {
                $cur_ev = $events[$date];

                if ($cur_ev->Days > 1) {
                    $ndate = ['start' => $cur_ev->DateFrom.' 00:00:00', 'end' => $cur_ev->DateTo.' 23:59:59'];
                    $date_skip_count = $cur_ev->Days;

                } else {
                    $ndate = ['start' => $cur_ev->DateFrom];
                    $date_skip_count = 0;
                }

                $rd[] = array_merge($ndate, ['allDay' => false, 'title' => strtoupper($cur_ev->ShortName), 'className' => 'slot3 sp_event red lighten-1']);

                continue;
            }

            if ($date_skip_count != 0) { $date_skip_count--; continue; } 

            if (strtotime($date) <= strtotime($today)) { continue; }

            if (!in_array($day, ['Sat', 'Sun'])) {

                // if (!empty($events[$date])) {
                //     $cur_ev = $events[$date];

                //     if ($cur_ev->Days > 1) {
                //         $ndate = ['start' => $cur_ev->DateFrom, 'end' => $cur_ev->DateTo];
                //         $date_skip_count = $cur_ev->Days;

                //     } else {
                //         $ndate = ['start' => $cur_ev->DateFrom];
                //         $date_skip_count = 0;
                //     }

                //     $rd[] = array_merge($ndate, ['title' => strtoupper($cur_ev->ShortName), 'className' => 'slot3 sp_event red lighten-1']);

                // } else {
                    $day_count++;
    
                    if ($day_count <= $this->days_interval) { continue; }
    
                    $sched_count = !empty($sched[$date]) ? count($sched[$date]) : 0;
                    $slot = ($this->max_slot - $sched_count);
    
                    $rd[] = [
                            'start' => $date,
                            'title' => ($slot <= 0) ? 'FULL' : $slot,
                            'className' => 'slot1 light-blue darken-3',
                        ];
    
                    if (in_array($date, $reserve_dt)) {
                        $stat = ($reserve[$date]['cancelled'] == 1) ? 'CANCELLED' : 'SAVED';
    
                        $rd[] = [
                                'start' => $date,
                                'title' => $stat,
                                'className' => 'slot2 grey',
                            ];
                    }

                // }
                
            }
        }

        return $rd;
    }

    public static function genRefNo()
    {
        $rand = str_random(7);
        $find = self::where('ref_no', $rand)->first();

        if (!empty($find)) {
            self::genRefNo();
        } else {
            return strtoupper($rand);
        }
    }

    public function getMonthEvents($month, $year)
    {
        $q = DB::table('occu_sched_events as se')->select('se.id', 'se.DateFrom', 'se.DateTo', 'se.Days', 'se.Caption', 'et.ShortName', 'et.Name')
                ->join('occu_event_types as et', 'et.id', '=', 'se.EventTypeID')
                ->get()->keyBy('DateFrom')->toArray();

        return $q;

    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pages extends Model
{
    protected $table = 'BUS_ExecutivePage';
    protected $primaryKey = 'PageID';

    protected $timestamp = false;

    protected $fillable = array('Name', 'Label', 'Url', 'Icon', 'InlineClass', 'IsParent', 'ParentPageID', 'Sorting', 'Inactive');

    public function navs()
    {
        $res = [];
        $pages = $this->where(['Inactive' => 0, 'IsParent' => 1])->orderBy('Sorting', 'asc')->get();

        foreach ($pages as $nav) {
            $has_sub = $this->where(['ParentPageID' => $nav->PageID, 'Inactive' => 0])->count();

            if($has_sub >= 1){
                $sub = $this->where(['ParentPageID' => $nav->PageID, 'Inactive' => 0])->orderBy('Sorting', 'asc')->get();
                $subs = [];

                foreach($sub as $sb){

                    $subs[] = array(
                        'sub_id' => $sb->PageID,
                        'module' => $sb->Name,
                        'label' => $sb->Label,
                        'url' => $sb->Url,
                        'icon' => $sb->Icon,
                    );
                }

                $data = array(
                    'module' => $nav->Name,
                    'label' => $nav->Label,
                    'url' => $nav->Url,
                    'icon' => $nav->Icon,
                    'sub' => $subs
                );

                $res[$nav->PageID] = $data;

            } else {
                $data = array(
                    'module' => $nav->Name,
                    'label' => $nav->Label,
                    'url' => $nav->Url,
                    'icon' => $nav->Icon,
                );

                $res[$nav->PageID] = $data;

            }
        }

        // print_result($res);
        // die();

        return $res;
    }

    public function filteredNavs()
    {
        $navs = [];

        $pages = DB::table($this->table.' as p')->select(DB::raw('distinct p.PageID'), 'p.*')
                    ->join('roles_access as ra', 'ra.PageID', '=', 'p.PageID')
                    ->where(['p.Inactive' => 0, 'ra.roleID' => logged_user_role()])
                    // ->groupBy('ra.PageID')
                    ->orderBy('p.Sorting', 'asc')
                    // ->toSql();
                    ->get();

        foreach ($pages as $p) {
            if ($p->IsParent) {
                $navs[$p->PageID] = [
                    'module' => $p->Name,
                    'label' => $p->Label,
                    'url' => $p->Url,
                    'icon' => $p->Icon,
                ];

            } else {

                if (empty($navs[$p->ParentPageID])) {
                    $nd = $this->where('PageID', $p->ParentPageID)->first();

                    $navs[$p->ParentPageID] = [
                        'module' => $nd->Name,
                        'label' => $nd->Label,
                        'url' => $nd->Url,
                        'icon' => $nd->Icon,
                    ];
                }

                $navs[$p->ParentPageID]['sub'][] = [
                    'sub_id' => $p->PageID,
                    'module' => $p->Name,
                    'label' => $p->Label,
                    'url' => $p->Url,
                    'icon' => $p->Icon,
                ];

            }
        }

        // dd($navs);
        return $navs;
    }

    public function access_has($page_name, $actions = [], $actions_by_id = false)
    {
        $f = ($actions_by_id) ? 'pa.id' : 'pa.action';

        $g = DB::table($this->table.' as p')
                ->join('roles_access as ra', 'ra.PageID', '=', 'p.PageID')
                ->join('pages_actions as pa', 'ra.ActionID', '=', 'pa.ID')
                ->where(['p.Name' => $page_name, 'ra.RoleID' => logged_user_role()])
                ->whereIn($f, $actions)
                // ->get();
                ->count();

        $g = ($g) ?: false;
        return $g;
    }
}

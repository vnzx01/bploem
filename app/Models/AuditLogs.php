<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AuditLogs extends Model
{
    protected $table = 'audit_logs';
    protected $primaryKey = 'id';

    protected $fillable = ['type', 'action', 'performed_on', 'page_name', 'properties', 'logged_by', 'logged_at'];

    public $timestamps = false;

    public function getRecords()
    {
        $d = DB::table($this->table.' as al')->select(DB::raw('SQL_CALC_FOUND_ROWS al.*'), 'p.Label', 'p.PageID', DB::raw('CONCAT(u.lastname, ", ", u.firstname, " ", CASE u.mi WHEN "" THEN null ELSE CONCAT(u.mi, ".") END) as fullname'))
                ->join('pages as p', 'p.Name', '=', 'al.page_name')
                ->join('users as u', 'u.id', '=', 'al.logged_by');
                // ->offset($offset)
                // ->limit($limit)
                // ->get();

        return $d;
    }

    // This function must run after the query to catch the total records of current query
    public function getQueryTotal()
    {
        $t = $this->select(DB::raw('FOUND_ROWS() as total'))->first()->toArray();
        return $t['total'];
    }
    // End
}

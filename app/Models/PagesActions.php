<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagesActions extends Model
{
    protected $table = 'pages_actions';
    protected $primaryKey = 'id';

    protected $fillable = ['action', 'default', 'BelongsTo_PageID'];

    public $timestamps = false;

}

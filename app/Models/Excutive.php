<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Executive extends Model
{

    protected $table = 'department';
    protected $primaryKey = 'id';

    protected $fillable = ['department_code', 'department_name', 'department_desc', 'created_by', 'updated_by'];

}

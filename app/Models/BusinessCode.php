<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessCode extends Model
{
    protected $table = 'businesscode';
    protected $primaryKey = 'ID';

    protected $fillable = ['BIN', 'BUS_Code'];

}

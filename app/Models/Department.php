<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CustomModel;

class Department extends Model
{
    use CustomModel;

    protected $table = 'department';
    protected $primaryKey = 'id';

    protected $fillable = ['department_code', 'department_name', 'department_desc', 'created_by', 'updated_by'];

}

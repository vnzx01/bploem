<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = ['email', 'password', 'remember_token', 'type_of_application', 'created_at', 'updated_at', 'lastname', 'firstname', 'middlename', 'mi', 'address', 'house_t', 'lot_t', 'block_t', 'street_t', 'area_t', 'brgy_t', 'city_t', 'dob', 'age', 'pob', 'nationality', 'gender', 'cstat', 'cont_num', 'religion', 'height', 'weight', 'hair_color', 'eye_color', 'identifying_marks' , 'blood_type', 'sss', 'tin', 'occupation', 'is_foodhandler', 'em_cont_person' , 'em_cont_address' , 'em_cont_num', 'business_bin', 'positionid', 'departmentid', 'programid', 'roleid', 'super_admin', 'profile_complete', 'email_sent', 'is_verified', 'verification_token', 'ACR_No'];

    protected $hidden = ['password', 'remember_token'];

    public function getRecords($id = 0)
    {
        $g = DB::table($this->table.' as u')->select('u.*', 'd.department_code as DeptCode', 'd.department_name as DeptName', 'p.position_code as PosCode', 'p.position_name as PosName')
                ->join('department as d', 'd.id', '=', 'u.departmentID')
                ->join('position as p', 'p.id', '=', 'u.positionID');

        $r = (!empty($id)) ? $g->where('u.id', $id)->first() : $g->get();

        return $r;
    }
}

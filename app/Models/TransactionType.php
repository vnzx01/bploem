<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Traits\CustomModel;

class TransactionType extends Model
{
    // use CustomModel;

    protected $table = 'trans_type';
    protected $primaryKey = 'id';

    protected $fillable = ['trans_name'];

}

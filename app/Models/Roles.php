<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CustomModel;

class Roles extends Model
{
    use CustomModel;

    protected $table = 'roles';
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'description'];

}

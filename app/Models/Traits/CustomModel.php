<?php
namespace App\Models\Traits;

trait CustomModel
{
    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
            $model->created_by = logged_id();
        });

        static::updating(function($model){
            $model->updated_by = logged_id();
        });
    }
}

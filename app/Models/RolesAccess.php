<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolesAccess extends Model
{
    protected $table = 'roles_access';
    protected $primaryKey = 'id';

    protected $fillable = ['RoleID', 'PageID', 'ActionID'];

    public $timestamps = false;
}

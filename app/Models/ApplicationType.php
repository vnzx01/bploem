<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Traits\CustomModel;

class ApplicationType extends Model
{
    // use CustomModel;

    protected $table = 'app_type';
    protected $primaryKey = 'id';

    protected $fillable = ['app_name'];

}

<div class="row">
    <div class="col m8 s12">
        <div class="card">
            <div class="card-content">
                <span class="card-title header-color"><i class="material-icons small circle white-text z-depth-1">timeline</i>Employee</span>
                <div style="width:100%;">
                    <canvas id="canvas"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

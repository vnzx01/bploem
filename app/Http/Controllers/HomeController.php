<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Schedules;

class HomeController extends Controller
{
    private $views = 'dashboard.views.executive';

    private $assoc_page_name = 'usr_profile'; //name must be based on pages created, used on permission
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guest()) {
            return redirect('/login');

        } else {

            $data = [
                'assoc_page_name' => $this->assoc_page_name,
                'plugins' => ['select2', 'datatables'],
                'views' => $this->views,
                // 'data' => $user,
            ];

            return view('layouts.core', $data);

        }

    }
}

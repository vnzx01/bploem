<?php

namespace App\Http\Controllers\Auth;

use App\User;
// use App\Mail\RegsEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
// use Illuminate\Support\Facades\Mail;
use App\Notifications\RegistrationSendMail as RegsMailNotif;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return redirect('/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $first = $data['firstname'];
        $last = $data['lastname'];

        $regex = '/^[a-zA-Z .]+$/';

        $v = Validator::make($data, [
            'firstname' => 'required|regex:'.$regex,
            'lastname' => 'required|regex:'.$regex,
            'middlename' => 'nullable|regex:'.$regex,
            'gender' => 'required|in:m,f',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'date-of-birth' => 'required|date',
        ], [], ['date-of-birth' => 'date of birth']);

        $v->after(function($rv) use ($first, $last) {
            $f = User::where(['lastname' => $last, 'firstname' => $first])->first();

            if (!empty($f)) {
                $rv->errors()->add('fullname', "You're already registered.");
            }
        });


        return $v;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $mi = !empty($data['middlename']) ? implode('', array_map(function($ar) { return $ar[0]; }, explode(' ', $data['middlename']))) : '';

        $dob = strtotime($data['date-of-birth']);
        $age = floor((time() - $dob) / 31556926);

        $user = User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'middlename' => $data['middlename'],
            'mi' => $mi,
            'gender' => $data['gender'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'verification_token' => str_random(40),
            'dob' => date('Y-m-d', $dob),
            'age' => $age,
        ]);

        // Mail::to($data['email'])->send(new RegsEmail($user));

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        // send email
        $user->notify(new RegsMailNotif($user));
        // custom response
        return response()->json(['error' => false, 'message' => "Congratulations! You're now registered. Please check on your email's inbox or spam folder for verification. Thank you."]);
    }

    // Custom function for verification_token
    public function verify($code)
    {
        $msg = [];

        $v = Validator::make(['verification_code' => $code], [
            'verification_code' => 'required|alpha_num',
        ]);

        $err = $v->messages();

        if (!empty($err->messages)) {
            $msg = ['title' => 'Oops!', 'color' => 'red', 'msg' => 'Invalid verification code. Back to <a href="'.url('/').'">login.</a>'];

        } else {
            $g = User::where('verification_token', $code)->first();

            if (!empty($g)) {
                if ($g->is_verified) {
                    $msg = ['title' => 'Sorry', 'color' => 'red', 'msg' => 'Verification code already used. Back to <a href="'.url('/').'">login</a>'];
                    
                } else {
                    User::where('verification_token', $code)->update(['is_verified' => 1]);
                    $msg = ['title' => 'Congratulations!', 'color' => 'green', 'msg' => 'You have successfully verify your email. Kindly login with your registered account <a href="'.url('/').'">here</a>.'];
                }
                
            } else {
                $msg = ['title' => 'Verification Code Error', 'color' => 'red', 'msg' => "Verification code doesn't exist."];

            }

        }

        $vw = view('auth.registration.verify', $msg);

        return $vw;
    }
    // End
}

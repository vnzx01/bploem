<?php

function logged_id()
{
    return Auth::user()->id;
}

function logged_name()
{
    $u = Auth::user();

    if (Auth::guard('staff')->check()) {
        $m = str_replace('-', '', $u->emp_mname);
        $mi = !empty($m) ? implode('', array_map(function($ar) { return $ar[0]; }, explode(' ', $m))) : '';
        $name = gen_fullname($u->emp_lname, $u->emp_fname, $mi);
        
    } else {
        $name = gen_fullname($u->lastname, $u->firstname, $u->mi);

    }
    return $name;
}

// function logged_program()
// {
//     $prog_id = Auth::user()->programID;
//
//     if(!empty($prog_id)) {
//         $pr = App\Models\Programs::select('ApplicationShortName')->where('id', $prog_id)->first();
//         $pr = $pr->ApplicationShortName;
//
//     } else {
//         $pr = '';
//     }
//
//     return $pr;
// }

function logged_user_role()
{
    return Auth::user()->roleID;
}

function is_super_admin()
{
    return Auth::user()->super_admin;
}

function grant_access($page_name, $actions, $action_by_id = false)
{
    if (is_super_admin()) {
        return true;
    } else {
        $p = new App\Models\Pages;

        $test = $p->access_has($page_name, $actions, $action_by_id);

        return ($test) ?: abort(401, 'Unauthorized.');
    }
}

/**
 * [app_log description]
 * @param  [type] $type            Use 'access' for interactions like viewing and getting details. Use 'transaction' for database interaction like create, update and delete.
 * @param  [type] $action          Action used in the controller (e.g. view, add, edit, view-data).
 * @param  [type] $namespace_class Namespace with class name to track what file is used. (e.g. App\Http\Controllers\settings\Department).
 * @param  [type] $assoc_page_name Page unique name (reffered to pages table) to track what page is being accessed.
 * @param  array  $data            Data to be inserted or updated or any. Data must be array.
 * @return $save
 */
function app_log($type, $action, $namespace_class, $assoc_page_name, $data = array())
{
    $al = new App\Models\AuditLogs;

    $d = [
        'type' => $type,
        'action' => $action,
        'performed_on' => $namespace_class,
        'page_name' => $assoc_page_name,
        'properties' => json_encode($data),
        'logged_by' => logged_id(),
        'logged_at' => date('Y-m-d H:i:s'),
    ];

    $save = $al->insert($d);

    return $save;
}

function link_active($string1, $string2)
{
    if (empty($string1) || empty($string2)) {
        return false;
    }

    return ($string1 == $string2) ? 'active' : false;
}

function gen_fullname($last, $first, $mi)
{
    $mi = ($mi) ? $mi.'.' : '';

    $full = ucwords(strtolower($last.', '.$first.' '.$mi));

    return $full;
}

function print_result($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}

function loading_spinner($class = null, $color = null, $state = 'hide', $id = 'loader-div')
{
    $class = !empty($class) ? $class : '';
    $color = !empty($color) ? $color : 'blue';

    $preloader = '<div class="loader-container '.$class.' '.$state.'" id="'.$id.'">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-'.$color.'-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>';

    return $preloader;
}

function return_value($object, $object_key, $default = '')
{
    $return  = '';

    if (is_object($object)) {
        $return = isset($object->$object_key) ? $object->$object_key : $default;
    } else {
        $return = isset($object[$object_key]) ? $object[$object_key] : $default;
    }

    return $return;
}

function return_date_value($object, $object_key, $date_format)
{
    $test = '';
    if (!empty($object)) {
        if (is_object($object)) {
            $object = (array) $object;
        }
        $test = isset($object[$object_key]) ? $object[$object_key] : '';
    }

    if (!empty($test)) {
        $return = date($date_format, strtotime($test));
    } else {
        if (isset($date_format)) {
            $return = date($date_format);
        } else {
            $return = date('Y-m-d H:i');
        }
    }

    return $return;
}

function return_selected_options($array, $value, $text, $compare_value = null, $attr = array(), $default = '')
{
    $options = '';
    if (!empty($array)) {
        if (is_object($array)) {
            $array = (array) $array;
        }

        for ($a = 0; $a < count($array); ++$a) {
            $attr_data = '';
            if (!empty($attr)) {
                foreach ($attr as $key => $attr_value) {
                    $attr_data .= 'data-'.$key.'="'.$array[$a]->$attr_value.'" ';
                }
            }
            $options .= '<option value="'.$array[$a]->$value.'" '.$attr_data.''.return_compare($array[$a]->$value, $compare_value, 'selected').'>'.$array[$a]->$text.'</option>';
        }
    } else {
        if (!empty($default)) {
            $options = '<option value="">'.$default.'</option>';
        } else {
            $options = '<option value="">Nothing to show.</option>';
        }
    }

    return $options;
}

function return_compare($str1, $str2, $return)
{
    if ($str1 == $str2) {
        return $return;
    } else {
        return null;
    }
}

function load_plugins($name = array())
{
    $r = [];

    if (!empty($name)) {
        $def_js = 'js/plugins/';
        $def_css = 'css/';

        $plugins = [
            'datatables' => [
                'js' => ['jquery.dataTables.min'],
                'css' => ['jquery.dataTables.min'],
            ],
            'select2' => [
                'js' => ['select2.min'],
                'css' => ['select2.min'],
            ],
			'fullcalendar' => [
                'js' => ['moment.min', 'fullcalendar.min'],
                'css' => ['fullcalendar'],
            ],
        ];

        foreach ($name as $n) {
            if (!empty($plugins[$n])) {

                foreach($plugins[$n]['css'] as $k => $v) {
                    $r['css'][] = '<link type="text/css" rel="stylesheet" href="'.asset($def_css.$plugins[$n]['css'][$k].'.css').'">';
                }

                foreach($plugins[$n]['js'] as $k => $v) {
                    $r['js'][] = '<script type="text/javascript" src="'.asset($def_js.$plugins[$n]['js'][$k].'.js').'"></script>';
                }
            }
        }

    }

    return $r;
}

function current_DateTime()
{
    return date('Y-m-d H:i:s');
}

function computed_days($dfrom, $dto)
{
    $days = 0;

    do {
        $dfrom = date('Y-m-d', strtotime($dfrom.' +1 day'));
        $day_name = date('D', strtotime($dfrom));

        // skip weekends on count
        if (!in_array($day_name, ['Sat', 'Sun'])) {
            $days++;
        }

    } while ($dfrom != $dto);

    return $days;

}
